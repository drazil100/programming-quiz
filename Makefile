PKG=quiz

all: ProgrammingQuiz.jar

clean:
	rm -f ProgrammingQuiz.jar
	rm -rf classes

ProgrammingQuiz.jar: $(patsubst %.java, classes/%.class, $(wildcard $(PKG)/*.java) $(wildcard $(PKG)/expressions/*.java)) Makefile
	jar -cfe $@ quiz.ProgrammingQuiz -C classes quiz

classes/%.class: %.java
	@mkdir -p classes
	javac -d classes -Xlint:unchecked $< 

test: ProgrammingQuiz.jar
	java -jar ProgrammingQuiz.jar

test-repeated: ProgrammingQuiz.jar
	cd classes && java quiz.RepeatedTest

autotest: ProgrammingQuiz.jar compare-test.sh
	bash compare-test.sh

autotest-repeated: ProgrammingQuiz.jar compare-test.sh
	while bash compare-test.sh; do false; done

zip:
	zip -9r /home/austin/public_html/ProgrammingQuiz.zip ProgrammingQuiz.jar README.txt quiz -x quiz/Makefile quiz/RepeatedTest.java
