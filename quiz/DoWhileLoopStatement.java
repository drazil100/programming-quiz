package quiz;

import quiz.expressions.*;
import java.util.*;

public class DoWhileLoopStatement extends ProgrammingStatement
{
	public ProgrammingExpression condition = null;
	public ProgrammingStatement body = null;

	public DoWhileLoopStatement(ProgrammingStatement p, int level)
	{
		indentLevel = level;
		parent = p;
	}

	@Override
	public String toString()
	{
		return indent() + "do\n" + body + indent() + "while (" +condition + ");\n";
	}

	@Override
	public void execute()
	{
		do
		{
			body.variables.clear();
			body.execute();

			if (getValue("@hasReturn") != 0)
				break;
		}
		while(condition.evaluate(this) != 0);
	}
}
