package quiz;

import quiz.expressions.*;
import java.util.*;

public class ExpressionStatement extends ProgrammingStatement
{
	ProgrammingExpression value;

	public ExpressionStatement(ProgrammingStatement p, int level, ProgrammingExpression v)
	{
		indentLevel = level;
		parent = p;
		value = v;
	}

	@Override 
	public void execute()
	{
		value.evaluate(parent);
	}

	@Override 
	public String toString()
	{
		String output = "";

		output += indent() + value + ";\n";

		return output;
	}
}
