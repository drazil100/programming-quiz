package quiz;

import quiz.expressions.*;
import java.util.*;

public class ForLoopStatement extends ProgrammingStatement
{
	public ProgrammingStatement initializer = null;
	public ProgrammingExpression condition = null;
	public ProgrammingStatement increment = null;
	public ProgrammingStatement body = null;

	public ForLoopStatement(ProgrammingStatement p, int level)
	{
		indentLevel = level;
		parent = p;
	}

	@Override
	public String toString()
	{
		String output = indent() + "for (";

		if (initializer == null)
			output += " ; ";
		else
			output += initializer.toString().trim() + " ";

		if (condition == null)
			output += "; ";
		else
			output += condition + "; ";

		if (increment != null)
			output += increment.toString().trim().replace(";", "");

		output += ")\n";

		if (body != null)
			output += body;
		else
			output += indent() + "\t{}\n";

		return output;
	}

	@Override
	public void execute()
	{
		if (initializer != null)
			initializer.execute();

		int runaway = 0;
		while(condition == null || condition.evaluate(this) != 0)
		{
			//body.variables.clear();
			body.execute();

			if (getValue("@hasReturn") != 0)
				break;

			if (increment != null)
				increment.execute();

			if(++runaway > 100) System.exit(1);
		}
	}
}
