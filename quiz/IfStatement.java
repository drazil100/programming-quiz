package quiz;

import quiz.expressions.*;
import java.util.*;

public class IfStatement extends ProgrammingStatement
{
	public ProgrammingExpression condition;
	public ProgrammingStatement ifBlock;
	public ProgrammingStatement elseBlock;

	public IfStatement(ProgrammingStatement p, int level,ProgrammingExpression conditionToTest)
	{		
		indentLevel = level;
		parent = p;
		condition = conditionToTest;
	}

	@Override 
	public void execute()
	{
		if (condition.evaluate(this) != 0)
		{
			ifBlock.execute();
		}
		else if(elseBlock != null)
		{
			elseBlock.execute();
		}
	}

	@Override 
	public String toString()
	{
		String output = "";
		String indents = indent();

		output += indents + "if (" + condition + ")\n";
		output += ifBlock;
		
		if (elseBlock != null)
		{
			output += indents + "else\n";
			output += elseBlock;
		}

		return output;
	}
}
