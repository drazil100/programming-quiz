package quiz;

import quiz.expressions.*;
import java.util.*;

public class IntDeclarationStatement extends ProgrammingStatement
{
	public String name;
	private ProgrammingExpression value;

	public IntDeclarationStatement(ProgrammingStatement p, int level, String n, ProgrammingExpression v)
	{
		indentLevel = level;
		parent = p;
		name = n;
		value = v;
	}
	
	@Override
	public void execute()
	{
		parent.declareVariable(name, value.evaluate(this));
	}

	@Override
	public String toString()
	{
		return (indent() + "int " + name + " = " + value + ";\n");
	}
}
