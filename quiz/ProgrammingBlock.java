package quiz;

import java.util.*;

public class ProgrammingBlock extends ProgrammingStatement
{
	public List<ProgrammingStatement> statements = new ArrayList<ProgrammingStatement>();

	public ProgrammingBlock(ProgrammingStatement p, int level)
	{
		indentLevel = level;
		parent = p;
	}

	@Override 
	public String toString()
	{
		String output = "";

		String indents = indent();
		output += indents + "{\n";
		for (int i = 0; i < statements.size(); i++)
			output += statements.get(i);
		output += indents + "}\n";

		return output;
	}

	@Override
	public void execute()
	{
		for (int i = 0; i < statements.size(); i++)
		{
			statements.get(i).execute();
			if(getValue("@hasReturn") != 0) return;
		}
	}
}
