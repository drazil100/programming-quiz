/*  Austin Allman */
package quiz;

import quiz.expressions.*;
import java.util.*;

class ProgrammingQuiz
{
	public static void main(String[] args) 
	{
		/*
		  REALLY IMPORTANT AND COMPLICATED
		
		  This class is responsible for generating
		  the problems and is responsible for making
		  sure that all code generated will run to completion
		  before giving it to the test taker
		
		  I do not expect you to figure out how this works
		  (heck I barely do)
		  */
		ProblemGenerator gen = new ProblemGenerator();

		int totalCorrect = 0;

		//  Generate problems
		QuizFunction[] problems = { 
			gen.generateProblem(1,0), 
			gen.generateProblem(2,0), 
			gen.generateProblem(2,1),
			gen.generateProblem(3,1),
			gen.generateProblem(3,2)
		};

		//  Sort problems by difficulty
		Arrays.sort(problems);

		Scanner input = new Scanner(System.in);

		System.out.print("\n");
		for (int i = 0; i < problems.length; i++)
		{
			problems[i].fnName += (i + 1);

			//  ProgrammingStatements all override toString()
			//  and use that to format what their output looks like
			System.out.println(problems[i]);

			//  ProgrammingStatements have an abstract execute() method
			//  that all classes that inherit from it use to run the code
			//  they represent
			problems[i].execute();

			//  QuizFunctions have a returnValue() method
			//  used to pull out the value returned by it.
			//
			//  Since execute() returns void this is needed to
			//  pull the value stored by ReturnStatement out 
			int value = problems[i].returnValue();
	
			int correctAnswer = (int)(Math.random() * 4) + 1;  // Randomly pick which of the 4 choices is the correct answer
			Set<Integer> answers = new HashSet<Integer>();     // list of answers already generated
			answers.add(correctAnswer);                        // add correct answer to list

			//  list anwer choices
			for(int j = 1; j <= 4; j++) {
				if(j == correctAnswer) 
				{
					System.out.println(j + ") " + value);  // write correct answer out
				} 
				else 
				{
					int offset;
					//  Make sure that no duplicate answers are generated
					do 
					{
						offset = (int)(Math.random() * 30) - 15;  // offset the correct answer to make believable incorrect answer
						if (offset < 1) offset--;                 // make sure the offset is not 0 (cause that would give us the correct answer
					}
					while(answers.contains(value + offset));
					answers.add(value + offset);                      // add answer to list
					System.out.println(j + ") " + (value + offset));  // print incorrect answers
				}
			}
			System.out.print("\nSelect your answer: ");  // make it clear the user is supposed to input something

			//  output based on correctness of the answer
			if (input.nextInt() == correctAnswer) 
			{
				System.out.println("Correct!");
				totalCorrect++;  // incriment the user's score
			}
			else
			{
				System.out.println("Incorrect. The correct answer is " + correctAnswer + ") " + value + ".");
			}
			System.out.print("\n");
		}

		System.out.print("You got " + totalCorrect + "/5 (" + (totalCorrect * 20) + "%) correct! You did ");

		//  figured a switch statement was the way to go.
		String output = "";
		switch (totalCorrect)
		{
			case 5: output = "Excellent!"; break;
			case 4: output = "Very Good!"; break;
			case 3: output = "Average."; break;
			default: output = "Poor...";
		}
		System.out.println(output);
	}
}  
