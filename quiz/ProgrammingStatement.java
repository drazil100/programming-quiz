package quiz;

import java.util.*;

public abstract class ProgrammingStatement
{
	protected ProgrammingStatement parent = null;                // The programming statment this statement belongs to
	protected Integer indentLevel = 0;                           // keep track of how deeply nested this statement is for display
	protected Map<String, Integer> variables = new HashMap<>();  // map of variables belonging to the scope of this programming statement

	//  declare variable
	public void declareVariable(String name, Integer initialValue)
	{
		variables.put(name, initialValue);
	}

	///////////////////////////////
	// below methods all recurse //
	///////////////////////////////

	//  undeclare variable
	public void undeclareVariable(String name)
	{
		if (!variables.containsKey(name) && parent != null)
			parent.undeclareVariable(name);
		else
			variables.remove(name);
	}

	//  set the value of an existing variable
	public void setValue(String name, int value)
	{
		if (!variables.containsKey(name) && parent != null)
			parent.setValue(name, value);
		else
			variables.put(name, value);
	}

	//  get value of an existing variable
	public Integer getValue(String name)
	{
		if (!variables.containsKey(name) && parent != null)
			return parent.getValue(name);
		else
			return variables.get(name);
	}

	//  get a list of variables of useable
	public List<String> availableVariables()
	{
		List<String> l = (parent == null) ? new ArrayList<String>() : parent.availableVariables();

		for (String variable : variables.keySet())
		{
			if (variable.charAt(0) != '@')
				l.add(variable);
		}

		return l;
	}

	//////////////////////////////
	// End of recursing methods //
	//////////////////////////////

	//  All methods inherited by this class
	//  use this method to run the represented code
	public abstract void execute();

	//  get indents for current statement
	public String indent()
	{
		String output = "";
		for (int i = 0; i < indentLevel; i++)
			output += "\t";
		return output;
	}
}
