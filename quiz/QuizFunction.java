package quiz;

import java.util.*;

public class QuizFunction extends ProgrammingBlock implements Comparable<QuizFunction>
{
	public String fnName;
	public int difficulty;

	public QuizFunction()
	{
		super((ProgrammingStatement)null, 0);
		this.fnName = "problem";
		declareVariable("@hasReturn", 0);
		declareVariable("@return", 0);
	}

	@Override
	public int compareTo(QuizFunction other)
	{
		return difficulty - other.difficulty;
	}

	@Override 
	public String toString()
	{
		String signature = "public static int " + fnName + "()\n";
		return signature + super.toString();
	}

	public Integer returnValue()
	{
		if (getValue("@hasReturn") == 0)
			return null;
		return getValue("@return");
	}
}
