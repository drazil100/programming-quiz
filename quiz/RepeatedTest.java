package quiz;

class RepeatedTest
{
	public static void main(String[] args)
	{
		ProblemGenerator gen = new ProblemGenerator();
		while(true)
		{
			QuizFunction fn = gen.generateProblem(3,3);
			fn.execute();
			System.out.println(fn);
			System.out.println("// " + fn.returnValue());
		}
	}
}
