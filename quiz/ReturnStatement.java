package quiz;

import quiz.expressions.*;
import java.util.*;

public class ReturnStatement extends ProgrammingStatement
{
	ProgrammingExpression value;

	public ReturnStatement(ProgrammingStatement p, int level, ProgrammingExpression v)
	{
		indentLevel = level;
		parent = p;
		value = v;
	}

	@Override 
	public void execute()
	{
		parent.setValue("@return", value.evaluate(parent));
		parent.setValue("@hasReturn", 1);
	}

	@Override 
	public String toString()
	{
		String output = "";

		output += indent() + "return " + value + ";\n";

		return output;
	}
}
