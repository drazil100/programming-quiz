package quiz;

import quiz.expressions.*;
import java.util.*;

public class WhileLoopStatement extends ProgrammingStatement
{
	public ProgrammingExpression condition = null;
	public ProgrammingStatement body = null;

	public WhileLoopStatement(ProgrammingStatement p, int level)
	{
		indentLevel = level;
		parent = p;
	}

	@Override
	public String toString()
	{
		return indent() + "while (" +condition + ")\n" + body;
	}

	@Override
	public void execute()
	{
		while(condition.evaluate(this) != 0)
		{
			body.variables.clear();
			body.execute();

			if (getValue("@hasReturn") != 0)
				break;
		}
	}
}
