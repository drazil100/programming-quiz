package quiz.expressions;

import java.util.*;

public class AddExpression extends BinaryExpression
{
	public AddExpression(ProgrammingExpression leftSide, ProgrammingExpression rightSide)
	{
		super("+", 11, leftSide, rightSide);
	}

	@Override
	protected Integer calculate(Integer leftSide, Integer rightSide)
	{
		return leftSide + rightSide;
	}
}
