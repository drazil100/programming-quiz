package quiz.expressions;

import quiz.*;
import java.util.*;

public class AssignmentExpression extends ProgrammingExpression
{
	protected String name;
	protected ProgrammingExpression value;

	public AssignmentExpression(String name, ProgrammingExpression value)
	{
		super(1);
		this.name = name;
		this.value = value;
	}

	@Override
	public Integer evaluate(ProgrammingStatement context)
	{
		Integer result = value.evaluate(context);
		context.setValue(name, result);
		return result;
	}

	@Override
	public String toString()
	{
		return name + " = " + value;
	}
}
