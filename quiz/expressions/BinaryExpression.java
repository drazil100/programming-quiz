package quiz.expressions;

import quiz.*;
import java.util.*;

public abstract class BinaryExpression extends ProgrammingExpression
{
	protected ProgrammingExpression leftSide, rightSide;
	
	protected String op;

	protected BinaryExpression(String op, Integer precedence, ProgrammingExpression leftSide, ProgrammingExpression rightSide)
	{
		super(precedence);
		this.op = op;
		this.leftSide = leftSide;
		this.rightSide = rightSide;
	}

	protected Integer calculate(Integer leftSide, Integer rightSide)
	{
		System.out.println("calculate not overridden for " + this.op);
		System.exit(1);
		return 0;
	}

	@Override
	public Integer evaluate(ProgrammingStatement context)
	{
		return calculate(leftSide.evaluate(context), rightSide.evaluate(context));
	}

	@Override
	public String toString()
	{
		String output = "";

		if(this.precedence > leftSide.precedence)
			output += "(" + leftSide + ")";
		else
			output += leftSide;

		output += " " + op + " ";

		// It is important that this is >= while the previous comparison is
		// just >. Consider a subtraction operation whose rightSide is another
		// subtraction operation. This must be written a - (b - c), because
		// a - b - c is (a - b) - c. If the subtraction is on the left side,
		// however, the parentheses are unnecessary due to left-to-right
		// evaluation.
		if(this.precedence >= rightSide.precedence)
			output += "(" + rightSide + ")";
		else
			output += rightSide;

		return output;
	}
}
