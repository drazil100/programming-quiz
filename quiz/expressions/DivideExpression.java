package quiz.expressions;

import java.util.*;

public class DivideExpression extends BinaryExpression
{
	public DivideExpression(ProgrammingExpression leftSide, ProgrammingExpression rightSide)
	{
		super("/", 12, leftSide, rightSide);
	}

	@Override
	protected Integer calculate(Integer leftSide, Integer rightSide)
	{
		return leftSide / rightSide;
	}
}
