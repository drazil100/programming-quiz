package quiz.expressions;

import java.util.*;

public class EqualsExpression extends BinaryExpression
{
	public EqualsExpression(ProgrammingExpression leftSide, ProgrammingExpression rightSide)
	{
		super("==", 8, leftSide, rightSide);
	}

	@Override
	protected Integer calculate(Integer leftSide, Integer rightSide)
	{
		return (leftSide == rightSide) ? 1 : 0;
	}
}
