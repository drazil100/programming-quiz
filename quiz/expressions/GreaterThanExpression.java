package quiz.expressions;

import java.util.*;

public class GreaterThanExpression extends BinaryExpression
{
	public GreaterThanExpression(ProgrammingExpression leftSide, ProgrammingExpression rightSide)
	{
		super(">", 9, leftSide, rightSide);
	}

	@Override
	protected Integer calculate(Integer leftSide, Integer rightSide)
	{
		return (leftSide > rightSide) ? 1 : 0;
	}
}
