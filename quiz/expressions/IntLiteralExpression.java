package quiz.expressions;

import quiz.*;
import java.util.*;

public class IntLiteralExpression extends ProgrammingExpression
{
	protected Integer value;

	public IntLiteralExpression(Integer value)
	{
		super(16);
		this.value = value;
	}

	public Integer evaluate(ProgrammingStatement context)
	{
		return value;
	}

	@Override public String toString()
	{
		return value.toString();
	}
}
