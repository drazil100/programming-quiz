package quiz.expressions;

import quiz.*;
import java.util.*;

public class LogicalAndExpression extends BinaryExpression
{
	public LogicalAndExpression(ProgrammingExpression leftSide, ProgrammingExpression rightSide)
	{
		super("&&", 4, leftSide, rightSide);
	}

	@Override
	public Integer evaluate(ProgrammingStatement context)
	{
		Integer lhs = leftSide.evaluate(context);
		if (lhs == 0)
			return 0;
		return (rightSide.evaluate(context) != 0) ? 1 : 0;
	}
}
