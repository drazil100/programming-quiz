package quiz.expressions;

import quiz.*;
import java.util.*;

public class LogicalNotExpression extends ProgrammingExpression
{
	protected ProgrammingExpression value;

	public LogicalNotExpression(ProgrammingExpression value)
	{
		super(13);
		this.value = value;
	}

	public Integer evaluate(ProgrammingStatement context)
	{
		Integer result = value.evaluate(context);
		if(result == 0)
			return 1;
		else
			return 0;
	}

	@Override public String toString()
	{
		if (this.precedence >= value.precedence)
			return "!(" + value + ")";
		else
			return "!" + value;
	}
}
