package quiz.expressions;

import quiz.*;
import java.util.*;

public class LogicalOrExpression extends BinaryExpression
{
	public LogicalOrExpression(ProgrammingExpression leftSide, ProgrammingExpression rightSide)
	{
		super("||", 3, leftSide, rightSide);
	}

	@Override
	public Integer evaluate(ProgrammingStatement context)
	{
		Integer lhs = leftSide.evaluate(context);
		if (lhs != 0)
			return 1;
		return (rightSide.evaluate(context) != 0) ? 1 : 0;
	}
}
