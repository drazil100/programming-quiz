package quiz.expressions;

import quiz.*;
import java.util.*;

public class MinusEqualsExpression extends ProgrammingExpression
{
	protected String name;
	protected ProgrammingExpression value;

	public MinusEqualsExpression(String name, ProgrammingExpression value)
	{
		super(1);
		this.name = name;
		this.value = value;
	}

	@Override
	public Integer evaluate(ProgrammingStatement context)
	{
		Integer oldValue = context.getValue(name);
		Integer result = oldValue - value.evaluate(context);
		context.setValue(name, result);
		return result;
	}

	@Override
	public String toString()
	{
		return name + " -= " + value;
	}
}
