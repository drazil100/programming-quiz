package quiz.expressions;

import java.util.*;

public class ModuloExpression extends BinaryExpression
{
	public ModuloExpression(ProgrammingExpression leftSide, ProgrammingExpression rightSide)
	{
		super("%", 12, leftSide, rightSide);
	}

	@Override
	protected Integer calculate(Integer leftSide, Integer rightSide)
	{
		return leftSide % rightSide;
	}
}
