package quiz.expressions;

import java.util.*;

public class NotEqualsExpression extends BinaryExpression
{
	public NotEqualsExpression(ProgrammingExpression leftSide, ProgrammingExpression rightSide)
	{
		super("!=", 8, leftSide, rightSide);
	}

	@Override
	protected Integer calculate(Integer leftSide, Integer rightSide)
	{
		return (leftSide != rightSide) ? 1 : 0;
	}
}
