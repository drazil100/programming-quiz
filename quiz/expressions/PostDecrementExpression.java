package quiz.expressions;

import quiz.*;
import java.util.*;

public class PostDecrementExpression extends ProgrammingExpression
{
	protected String name;

	public PostDecrementExpression(String name)
	{
		super(14);
		this.name = name;
	}

	@Override
	public Integer evaluate(ProgrammingStatement context)
	{
		Integer result = context.getValue(name);
		context.setValue(name, result - 1);
		return result;
	}

	@Override
	public String toString()
	{
		return name + "--";
	}
}

