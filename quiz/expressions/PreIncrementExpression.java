package quiz.expressions;

import quiz.*;
import java.util.*;

public class PreIncrementExpression extends ProgrammingExpression
{
	protected String name;

	public PreIncrementExpression(String name)
	{
		super(13);
		this.name = name;
	}

	@Override
	public Integer evaluate(ProgrammingStatement context)
	{
		Integer result = context.getValue(name) + 1;
		context.setValue(name, result);
		return result;
	}

	@Override
	public String toString()
	{
		return "++" + name;
	}
}

