package quiz.expressions;

import quiz.*;
import java.util.*;

public abstract class ProgrammingExpression
{
	// See http://www.cs.bilkent.edu.tr/~guvenir/courses/CS101/op_precedence.html
	// for a list of numeric precedence values. Variables and constants have
	// precedence 16.
	protected Integer precedence;

	public ProgrammingExpression(Integer precedence)
	{
		this.precedence = precedence;
	}

	public abstract Integer evaluate(ProgrammingStatement context);
}
