package quiz.expressions;

import java.util.*;

public class SubtractExpression extends BinaryExpression
{
	public SubtractExpression(ProgrammingExpression leftSide, ProgrammingExpression rightSide)
	{
		super("-", 11, leftSide, rightSide);
	}

	@Override
	protected Integer calculate(Integer leftSide, Integer rightSide)
	{
		return leftSide - rightSide;
	}
}
