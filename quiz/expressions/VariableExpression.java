package quiz.expressions;

import quiz.*;
import java.util.*;

public class VariableExpression extends ProgrammingExpression
{
	protected String name;

	public VariableExpression(String name)
	{
		super(16);
		this.name = name;
	}

	public Integer evaluate(ProgrammingStatement context)
	{
		return context.getValue(name);
	}

	@Override public String toString()
	{
		return name;
	}
}
